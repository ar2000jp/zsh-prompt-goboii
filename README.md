# Goboii
A minimal [Prezto](https://github.com/sorin-ionescu/prezto) Zsh prompt theme inspired by GoboLinux's default theme, among others.

<img src="goboii-screenshot.png" width="887">

### Install
- cd ~/.zprezto/modules/prompt/external
- git clone https://gitlab.com/ar2000jp/zsh-prompt-goboii.git goboii
- cd ~/.zprezto/modules/prompt/functions
- ln -s ~/.zprezto/modules/prompt/external/goboii/prompt_goboii_setup
- Add `prompt goboii` to your `~/.zpreztorc`.

### Configuration
The theme's colors can be configured by changing the line in `~/.zpreztorc` to something like:
- `prompt goboii white blue`

The syntax is:
- `prompt goboii [<prompt color> [<path color>]]`

Also, colors, parameters and behavior can be changed by editing the
file `config` in `~/.zprezto/modules/prompt/external`.

Support for dynamic colors is based on fletcher16 algorithm with the colors
clamped to 16 colors. Support for a more sophisticated color selection
algorithm (contrast, 256 colors, etc) might come in the future.

### License

This work is licensed under the MIT License.
